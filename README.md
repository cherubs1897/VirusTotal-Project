# VirusTotal-Project
In this Project we want to build a website that allows users to upload a text file(list of hashes) and then scan it for malware by querying VirusTotal's Public API and then generate a final report in HTML table format.

I can also make different files for integrating database and establishing a SMTP server and then integrate them in a final file but here I did all the code in one file and mentioned comments for better clarity so that user don't have to jump between files to understand the code, otherwise different files/classes could be made and code could be designed even better. 

Ques-The input list is very larges so users mat not be able to wait for such a long time and could not see the report right away, consider them coming back and viewing the report.

Answer-Total Virus only allows to scan us 4 files per minute so if it is queuing you or giving empty json() error that means you need to try again, In case the file size is really large; in my code I have specified it as 1mb(10^6 bytes) then I have decided ti implement the e-mail service where the email of results would be sent to the user after scanning the file.

Ques-Many people might use the website frequently over time. The same hash can be queried multiple times. Consider how to store VirusTotal's results locally so that the website does not need to query the same hash again from VirusTotal if the scan date is less than 1 day ago.

Answer: For such a scenario I have integrated my python script to NOSQL database i.e. MongoDB where the outputs would be stored and if user tries to scan the same file again, instead of querying it; the output would be retreived from database and would be presented to user on a html page in html table format. Output would be presented such that it would also contain the link to totalVirus Scan results.

Although for fewer outputs this can aslo be implemented using dictionary and list data structure in python.

All the query results would be stored in MongoDB for 24 hours using TTL(Time to Live) and after that they would be deleted automatically.

- Technology Stack Used-  
-                         Python(3.9.12), Spyder IDE
-                         MongoDB ATLAS(Cloud based database cluster),
-                         Windows,

- Required Dependancies and libraries
-                         Requests,json2html,webbrowser,datetime,os
-                         smtplib,
-                         pymongo,email,email.mime....


Steps:
- Have all the prerequisites as mentioned above
- Run the .py file
- Import all the dependancies mentioned in the file
- You can read the code comments for better understanding
- In the driver code in getsize function and deveopsprojectfunction: add the name of txt file you want to scan enclosed in ""
- ******Keep in mind the directory of the target file should be same as that of your .py file otherwise mention the directory or path to target file there**** in my code i renamed the hash file as my_file.txt and it was in same directory as my .py file
- After that Login to API server create your account, get yout API key and enter it after running the program, when prompted Enter your API key:
Or use my API key- b6cf4a1fab3cba32aa2886941eb3dde5c55e742b696cb33981afcd34230785f7
- After that run the program and you will be presented with results
- IF there is an empty.json() error or results queued that means website is not returning result or our query is queued respectively; so one can run after some time or give it a few tries.



